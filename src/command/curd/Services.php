<?php

declare(strict_types=1);

namespace cosy\framework\command\curd;

use cosy\framework\command\BaseMake;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;

/**
 * ClassName Service
 * Description TODO
 * Author BTC
 * Date 2023/11/3 22:29
 **/
class Services extends BaseMake
{
    protected $type = 'service';

    protected function configure()
    {
        $this->setName('cosy:service')
            ->addArgument('name', Argument::REQUIRED, 'Please input your class name')
            ->addArgument('mapper', Argument::REQUIRED, 'Please input your mapper name')
            ->setDescription('create a service');
    }

    protected function execute(Input $input, Output $output)
    {
        $name = trim($input->getArgument('name'));
        $mapper = trim($input->getArgument('mapper'));
        $this->build($name, $mapper);

        $output->writeln('<info>' . $this->type . ':' . $name . ' created successfully.</info>');
    }

    // 填充模板
    public function build($name, $mapper)
    {
        $stub = $this->getStub();
        $namespace = trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');
        $class = str_replace($namespace . '\\', '', $name);
        $className = $class . ucfirst($this->type);

        $mapperSapce = trim(implode('\\', array_slice(explode('\\', $mapper), 0, -1)), '\\');
        $mapperName = str_replace($mapperSapce . '\\', '', $mapper);

//        $namespaceDirName = substr($namespace, 0, strrpos($namespace, '\\'));

        $search = [
            '{%createTime%}',
            '{%mapperNameSpace%}',
            '{%className%}',
            '{%namespace%}',
            '{%mapperName%}'
        ];

        $replace = [
            date('Y-m-d H:i'),
            $mapper,
            $className,
            $namespace,
            $mapperName
        ];

        if (!file_exists($namespace)) {
            mkdir($namespace, 0777, true);
        }

        file_put_contents($this->getPathName($name), str_replace($search, $replace, $stub));

        return true;
    }

    /**
     * 获取模板
     * @return string
     */
    protected function getStub()
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . $this->type . '.stub';
        return file_get_contents($path);
    }

    protected function getClassName(string $name): string
    {
        if (strpos($name, '\\') !== false) {
            return $name;
        }

        if (strpos($name, '@')) {
            [$app, $name] = explode('@', $name);
        } else {
            $app = '';
        }

        if (strpos($name, '/') !== false) {
            $name = str_replace('/', '\\', $name);
        }

        return $this->getNamespace($app) . '\\' . $name;
    }

    protected function getPathName(string $name): string
    {
        $name = str_replace('app\\', '', $name);
        $name = $name . ucfirst($this->type);

        return $this->app->getBasePath() . ltrim(str_replace('\\', '/', $name), '/') . '.php';
    }
}