<?php

declare(strict_types=1);

namespace cosy\framework\command;

use think\console\command\Make;
use think\console\Input;
use think\console\Output;

/**
 * ClassName BaseMake
 * Description TODO
 * Author BTC
 * Date 2023/11/3 18:33
 **/
class BaseMake extends Make
{
    protected $type;

    /**
     * 获取模板
     * @return string
     */
    protected function getStub()
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . $this->type . '.stub';
        return file_get_contents($path);
    }

    /**
     * 获取类命名空间
     * @param string $appNamespace
     * @return string
     */
    protected function getNamespace($appNamespace): string
    {
        return parent::getNamespace($appNamespace) . '\\' . $this->type;
    }

    /**
     * @param Input $input
     * @param Output $output
     */
    public function executeBuild(Input $input, Output $output)
    {
        parent::execute($input, $output);
    }
}