<?php

namespace cosy\framework\traits;

trait CurdTrait
{
    public function index()
    {
        return $this->service->getPageList($this->request->all());
    }

    public function read($id)
    {
        $data = $this->request->all();
        $this->validate($data, $this->validateClass . '.' . __FUNCTION__);

        return $this->success('', $this->service->read((int)$data['id']));
    }

    public function save()
    {
        $data = $this->request->all();
        $this->validate($data, $this->validateClass . '.' . __FUNCTION__);

        return $this->success('', $this->service->save($data));
    }

    public function edit($id)
    {
        $data = $this->request->all();
        $this->validate($data, $this->validateClass . '.' . __FUNCTION__);

        return $this->success('', $this->service->edit((int)$data['id']));
    }

    public function update($id)
    {
        $data = $this->request->all();
        $this->validate($data, $this->validateClass . '.' . __FUNCTION__);

        return $this->success('', $this->service->update((int)$data['id']));
    }

    public function delete($id)
    {
        $data = $this->request->all();
        $this->validate($data, $this->validateClass . '.' . __FUNCTION__);

        return $this->success('', $this->service->delete((int)$data['id']));
    }
}